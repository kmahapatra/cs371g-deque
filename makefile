.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-10
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -g -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Deque.log:
	git log > Deque.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Deque code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Deque code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Deque.hpp
	-git add Deque.log
	-git add html
	git add makefile
	git add README.md
	git add TestDeque.cpp
	git commit -m "another commit"
	git push
	git status

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f TestDeque

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Deque.log
	rm -f  Doxyfile
	rm -rf deque-tests
	rm -rf html
	rm -rf latex

# compile test harness
TestDeque: Deque.hpp TestDeque.cpp
	-$(CPPCHECK) TestDeque.cpp
	$(CXX) $(CXXFLAGS) TestDeque.cpp -o TestDeque $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    TestDeque

# compile all
all: $(FILES)

# execute test harness
test: TestDeque
#	$(VALGRIND) ./TestDeque
	./TestDeque
	$(GCOV) TestDeque.cpp | grep -B 2 "cpp.gcov"

# test files in the Deque test repo
TFILES := `ls deque-tests/*.cpp`

# clone the Deque test repo
deque-tests:
	git clone https://gitlab.com/kmahapatra/cs371g-deque-tests.git deque-tests

deque-tests/%: deque-tests/%.cpp Deque.hpp
	-$(CPPCHECK) $@.cpp
	$(CXX) $(CXXFLAGS) -I. $@.cpp -o $@ $(LDFLAGS)

tests: deque-tests
	for v in `ls deque-tests/*.cpp`; do make $${v/.cpp/}; done
	for v in `ls deque-tests/*.cpp`; do echo "Testing $${v/.cpp/}"; ./$${v/.cpp/}; done

# auto format the code
format:
	$(ASTYLE) Deque.hpp
	$(ASTYLE) TestDeque.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Deque.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        \
    Deque.log                             \
    html

# check the existence of check files
check: $(CFILES)

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
