// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    bool eq = b == e;
      ASSERT_TRUE(eq);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    bool eq = b == e;
    ASSERT_TRUE(eq);
}

// --------
//  const <
// --------

TYPED_TEST(DequeFixture, clt0) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3, 4};
    const deque_type y {1, 2, 3, 5};
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, clt1) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {10};
    const deque_type y {5, 10};
    EXPECT_FALSE(x < y);
}

// --
//  <
// --

TYPED_TEST(DequeFixture, lt0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 5};
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, lt1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {10};
    deque_type y {5, 10};
    EXPECT_FALSE(x < y);
}

// ---
//  ==
// ---

TYPED_TEST(DequeFixture, eq0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4};
    deque_type y {1, 2, 3, 4};
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, eq2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {5};
    deque_type y {};
    EXPECT_FALSE(x == y);
}

// ------------
//  iterator ==
// ------------

TYPED_TEST(DequeFixture, it_eq0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {};
    EXPECT_TRUE(begin(x) == end(x));
}

TYPED_TEST(DequeFixture, it_eq1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {5, 10, 15, 20, 25};
    EXPECT_TRUE(begin(x) == begin(x));
}

// ------------
//  iterator 
// ------------

TYPED_TEST(DequeFixture, it0) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x {5, 10, 15, 20, 25};
    const deque_type y {5, 10, 15, 20, 25};
    auto begin_x = begin(x);
    auto begin_y = begin(y);
    ASSERT_EQ(*begin_x, *begin_y);
    ++begin_x;
    ++begin_y;
    ASSERT_EQ(*begin_x, *begin_y);
}

TYPED_TEST(DequeFixture, it1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator e = end(x);
    e--;
    --(*e);
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(*e, -1);
}

TYPED_TEST(DequeFixture, it2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5});
    iterator b = begin(x);
    b += 4;
    ASSERT_EQ(*b, 5);
}


// -----------
//  push_front
// -----------

TYPED_TEST(DequeFixture, pf0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {4, 5};

    x.push_front(3);
    x.push_front(2);
    x.push_front(1);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[4], 5);

    auto begin_x = begin(x);
    auto end_x = end(x);

    ASSERT_EQ(*begin_x, 1);
    ++begin_x;
    ASSERT_EQ(*begin_x, 2);
    ++begin_x;
    ASSERT_EQ(*begin_x, 3);
    ++begin_x;
    ASSERT_EQ(*begin_x, 4);
    ++begin_x;
    ASSERT_EQ(*begin_x, 5);
    ++begin_x;

    EXPECT_TRUE(begin_x == end_x);
}

// -----------
//  push_back
// -----------

TYPED_TEST(DequeFixture, pb0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2};

    x.push_back(3);
    ASSERT_EQ(x[2], 3);
    x.push_back(4);
    x.push_back(5);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[4], 5);

    auto begin_x = begin(x);
    auto end_x = end(x);

    --end_x;
    ASSERT_EQ(*end_x, 5);
    --end_x;
    ASSERT_EQ(*end_x, 4);
    --end_x;
    ASSERT_EQ(*end_x, 3);
    --end_x;
    ASSERT_EQ(*end_x, 2);
    --end_x;
    ASSERT_EQ(*end_x, 1);

    EXPECT_TRUE(begin_x == end_x);
}

// -------
//  erase
// -------

TYPED_TEST(DequeFixture, erase0) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.erase(b);
    EXPECT_TRUE(x.front() == 1);
}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.erase(b);
    EXPECT_TRUE(x.size() == 99);
}

// -------
//  insert
// -------
TYPED_TEST(DequeFixture, insert0) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    const initializer_list<int> x = {1, 3, 4, 5};
    deque_type y(x);
    iterator b = y.begin();
    ++b;
    iterator i = y.insert(b, 2);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(*i, 2);
    ASSERT_EQ(y.size(), 5);
}

TYPED_TEST(DequeFixture, insert1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.insert(x.begin(), 10);
    deque_type y = {10};
    ASSERT_TRUE(x == y);
}

// -----------------
//  copy constructor
// -----------------
TYPED_TEST(DequeFixture, constructor_copy0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3, 4, 5};
    const deque_type y(x);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
    ASSERT_EQ(y[3], 4);
    ASSERT_EQ(y[4], 5);
}

// ------------
//  operator =
// ------------

TYPED_TEST(DequeFixture, assignment0) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {};
    deque_type y {1, 2, 3, 4, 5};

    x = y;

    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, assignment1) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x {1, 2, 3, 4, 5};
    deque_type y = x;

    EXPECT_TRUE(x == y);
}

// ------
//  clear
// ------

TYPED_TEST(DequeFixture, clear0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {5, 10, 15, 20, 25};

    x.clear();
    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);

    x.push_front(10);
    ASSERT_EQ(x[0], 10);
    ASSERT_EQ(x.size(), 1);
}

// ---------
//  pop_back
// ---------

TYPED_TEST(DequeFixture, popback0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {1, 2, 3, 4, 5};

    ASSERT_EQ(x[x.size() - 1], 5);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 4);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 3);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 2);
    x.pop_back();
    ASSERT_EQ(x[x.size() - 1], 1);
    x.pop_back();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ----------
//  pop_front
// ----------

TYPED_TEST(DequeFixture, popfront0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {1, 2, 3, 4, 5};

    ASSERT_EQ(x[0], 1);
    x.pop_front();
    ASSERT_EQ(x[0], 2);
    x.pop_front();
    ASSERT_EQ(x[0], 3);
    x.pop_front();
    ASSERT_EQ(x[0], 4);
    x.pop_front();
    ASSERT_EQ(x[0], 5);
    x.pop_front();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  back
// ------

TYPED_TEST(DequeFixture, back0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {1, 2, 3, 4, 5};

    ASSERT_EQ(x.back(), 5);
    x.pop_back();
    ASSERT_EQ(x.back(), 4);
    x.pop_back();
    ASSERT_EQ(x.back(), 3);
    x.pop_back();
    ASSERT_EQ(x.back(), 2);
    x.pop_back();
    ASSERT_EQ(x.back(), 1);
    x.pop_back();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  front
// ------

TYPED_TEST(DequeFixture, front0) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x {1, 2, 3, 4, 5};

    ASSERT_EQ(x.front(), 1);
    x.pop_front();
    ASSERT_EQ(x.front(), 2);
    x.pop_front();
    ASSERT_EQ(x.front(), 3);
    x.pop_front();
    ASSERT_EQ(x.front(), 4);
    x.pop_front();
    ASSERT_EQ(x.front(), 5);
    x.pop_front();

    EXPECT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

// ------
//  swap
// ------

TYPED_TEST(DequeFixture, swap0)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x = {1, 2, 3};
    deque_type y = {1, 2, 3, 4, 5};
    swap(x, y);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
    ASSERT_EQ(x[3], 4);
    ASSERT_EQ(x[4], 5);
}

TYPED_TEST(DequeFixture, swap1)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x;
    deque_type y = {1, 2, 3, 4, 5};
    deque_type z = {1, 2, 3, 4, 5};
    swap(x, y);
    ASSERT_TRUE(x == z);
}


























