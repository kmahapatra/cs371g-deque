// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

#if 0
template<typename I1, typename I2>
bool my_equal(I1 first1, I1 last1, I2 first2) {
    for (; first1 != last1; ++first1, ++first2) {
        if (!(*first1 == *first2)) {
            return false;
        }
    }
    return true;
}
#endif

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * Check if contents of lhs and rhs are equal
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        return lhs.size() == rhs.size() && std::equal(lhs.begin(), lhs.end(), rhs.begin());
    }

    // ----------
    // operator <
    // ----------

    /**
     * Compare contents of lhs and rhs lexicographically
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * Exchange content of x with content of y
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    enum { kItemsPerChunk = 64 };

    size_t mask(size_t index) const {
        return index & (capacity_ - 1);
    }

    void EnsureRoomBack() {
        const Chunk* back = nullptr;
        // If there is no chunk or the last chunk is full, create a new chunk
        if (begin_ == end_ || (back = chunks_[mask(end_ - 1)], back->end - back->begin == kItemsPerChunk)) {
            NewBackChunk();
        }
    }

    void EnsureRoomFront() {
        const Chunk* front = nullptr;
        //If there is no chunk or the first chunk is full, create a new chunk
        if (begin_ == end_ || (front = chunks_[mask(begin_)], front->end - front->begin == kItemsPerChunk)) {
            NewFrontChunk();
        }
    }

    void NewBackChunk();
    void NewFrontChunk();


    // ----
    // data
    // ----

    // Using an union allows to keep uninitalized slots in 'Chunk'
    union PotentialItem {
        PotentialItem() {}
        ~PotentialItem() {}
        T data;
    };

    struct Chunk {
        PotentialItem items[kItemsPerChunk];
        // begin and end interpreted mod kItemsPerChunk
        unsigned begin = 0;
        unsigned end = 0;

        static size_t mask(size_t index) {
            return index & (kItemsPerChunk - 1);
        }

        void Destroy(A& alloc) {
            for (auto i = begin; i != end; ++i) {
                alloc.destroy(&items[mask(i)].data);
            }
            delete this;
        }
    };

    allocator_type a_;
    Chunk** chunks_ = nullptr;
    // begn,end interpreted mod capacity
    size_t begin_ = 0;
    size_t end_ = 0;
    size_t capacity_ = 0;

private:
    // -----
    // valid
    // -----

    bool valid () const {
        if (chunks_ == nullptr) {
            return begin_ == 0 && end_ == 0 && capacity_ == 0;
        }
        for (auto i = begin_; i != end_; ++i) {
            const auto* chunk = chunks_[mask(i)];
            if (i == begin_ || i == end_ - 1) {
                if (chunk->begin == chunk->end || chunk->end - chunk->begin > kItemsPerChunk) {
                    return false;
                }
            } else if (chunk->end - chunk->begin != kItemsPerChunk) {
                return false;
            }
        }
        return true;
    }


public:
    // --------
    // iterator
    // --------

    class iterator {
        friend class my_deque;

        // -----------
        // operator ==
        // -----------

        /**
         * Check if iterator lhs is equal to iterator rhs
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs.deque_ == rhs.deque_ && lhs.chunk_index_ == rhs.chunk_index_ &&
	      lhs.item_index_ == rhs.item_index_;
        }

        /**
         * Check if iterator lhs is not equal to iterator rhs
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * Advances iterator lhs by rhs element positions
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * Moves iterator lhs back by rhs element positions
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----
        iterator(my_deque* deque, size_t chunk_index, size_t item_index) : deque_(deque), chunk_index_(chunk_index), item_index_(item_index) {}
        iterator(my_deque* deque, size_t chunk_index) : deque_(deque), chunk_index_(chunk_index),
            item_index_(chunk_index == deque->end_ ? 0 : deque->chunks_[deque->mask(chunk_index)]->begin) {}



        my_deque* deque_ = nullptr;
        size_t chunk_index_ = 0;
        unsigned item_index_ = 0;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            if (deque_ == nullptr) {
                return chunk_index_ == 0 && item_index_ == 0;
            }
            if (chunk_index_ == deque_->end_) {
                return item_index_ == 0;
            }
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * Default constructor
         */
        iterator () {
            assert(valid());
        }

        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * Return reference to element at this position
         */
        reference operator * () const {
            assert(chunk_index_ != deque_->end_);
            auto* chunk = deque_->chunks_[deque_->mask(chunk_index_)];
            return chunk->items[chunk->mask(item_index_)].data;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * Return pointer to element at this position
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Increment position by 1 (prefix)
         */
        iterator& operator ++ () {
            ++item_index_;
            if (item_index_ == deque_->chunks_[deque_->mask(chunk_index_)]->end) {
                ++chunk_index_;
                item_index_ = chunk_index_ == deque_->end_ ? 0 : deque_->chunks_[deque_->mask(chunk_index_)]->begin;
            }
            assert(valid());
            return *this;
        }

        /**
         * Increment position by 1 (postfix)
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrement position by 1 (prefix)
         */
        iterator& operator -- () {
            if (chunk_index_ == deque_->end_ ||
                    item_index_ == deque_->chunks_[deque_->mask(chunk_index_)]->begin) {
                const auto* last_chunk = deque_->chunks_[deque_->mask(--chunk_index_)];
                item_index_ = last_chunk->end - 1;
            } else {
                --item_index_;
            }
            assert(valid());
            return *this;
        }

        /**
         * Decrement position by 1 (postfix)
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * Advance iterator by d element positions
         */
        iterator& operator += (difference_type d) {
            for (int i = 0; i < d; i++) {
                ++(*this);
            }
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * Move iterator back by d element positions
         */
        iterator& operator -= (difference_type d) {
            for (int i = 0; i < d; i++) {
                --(*this);
            }
            assert(valid());
            return *this;
        }
    };

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        friend class my_deque;
        // -----------
        // operator ==
        // -----------

        /**
         * Check if iterator object lhs is equal to iterator object rhs
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs.chunk_index_ == rhs.chunk_index_ && lhs.item_index_ == rhs.item_index_;
        }

        /**
         * Check if iterator object lhs is not equal to iterator object rhs
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * Advances iterator lhs by rhs element positions
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * Moves iterator lhs back by rhs element positions
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----
        const_iterator(const my_deque* deque, size_t chunk_index, size_t item_index) : deque_(deque), chunk_index_(chunk_index), item_index_(item_index) {}
        const_iterator(const my_deque* deque, size_t chunk_index) : deque_(deque), chunk_index_(chunk_index),
            item_index_(chunk_index == deque->end_ ? 0 : deque->chunks_[deque->mask(chunk_index)]->begin) {}


        // -----
        // valid
        // -----

        bool valid () const {
            if (deque_ == nullptr) {
                return chunk_index_ == 0 && item_index_ == 0;
            }
            if (chunk_index_ == deque_->end_) {
                return item_index_ == 0;
            }
            return true;
        }

        const my_deque* deque_ = nullptr;
        size_t chunk_index_ = 0;
        unsigned item_index_ = 0;

    public:
        // -----------
        // constructor
        // -----------

        /**
         * Default constructor
         */
        const_iterator () {
            assert(valid());
        }

        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * Return reference to element at this position
         */
        reference operator * () const {
            const auto* chunk = deque_->chunks_[deque_->mask(chunk_index_)];
            return chunk->items[chunk->mask(item_index_)].data;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * Return pointer to element at this position
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Increment position by 1 (prefix)
         */
        const_iterator& operator ++ () {
            ++item_index_;
            if (item_index_ == deque_->chunks_[deque_->mask(chunk_index_)]->end) {
                ++chunk_index_;
                item_index_ = chunk_index_ == deque_->end_ ? 0 : deque_->chunks_[deque_->mask(chunk_index_)]->begin;
            }
            assert(valid());
            return *this;
        }

        /**
         * Increment position by 1 (postfix)
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrement position by 1 (postfix)
         */
        const_iterator& operator -- () {
            if (chunk_index_ == deque_->end_ || item_index_ == deque_->chunks_[deque_->mask(chunk_index_)]->begin) {
                const auto* last_chunk = deque_->chunks_[deque_->mask(--chunk_index_)];
                item_index_ = last_chunk->end - 1;
            }
            else {
                --item_index_;
            }
            assert(valid());
            return *this;
        }

        /**
         * Decrement position by 1 (postfix)
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * Advance iterator by d element positions
         */
        const_iterator& operator += (difference_type d) {
            for (int i = 0; i < d; i++) {
                ++(*this);
            }
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * Move back iterator by d element positions
         */
        const_iterator& operator -= (difference_type d) {
            for (int i = 0; i < d; i++) {
                --(*this);
            }
            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // ------------

    my_deque () = default;

    /**
     * Fill constructor
     */
    explicit my_deque (size_type s) {
        for (size_type i = 0; i < s; ++i) {
            push_back(T());
        }
        assert(valid());
    }

    /**
     * Fill constructor with value v
     */
    my_deque (size_type s, const_reference v) {
        for (size_type i = 0; i < s; ++i) {
            push_back(v);
        }
        assert(valid());
    }

    /**
     * Fill constructor with value v and allocator a
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) : a_(a) {
        for (size_type i = 0; i < s; ++i) {
            push_back(v);
        }
        assert(valid());
    }

    /**
     * Constructor from initializer list
     */
    my_deque (std::initializer_list<value_type> rhs) {
        for (const auto& val : rhs) {
            push_back(val);
        }
        assert(valid());
    }

    /**
     * Constructor from initializer list and allocator a
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) : a_(a) {
        for (const auto& val : rhs) {
            push_back(val);
        }
        assert(valid());
    }

    /**
     * Copy constructor
     */
    my_deque (const my_deque& that) {
        for (const auto& val : that) {
            push_back(val);
        }
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * Destroy this object
     */
    ~my_deque () {
        clear();
        delete [] chunks_;
        chunks_ = nullptr;
        capacity_ = 0;
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * Replace current contents with new contents from rhs
     */
    my_deque& operator = (const my_deque& rhs) {
        if (&rhs != this) {
            this->~my_deque();
            new (this) my_deque(rhs);
        }
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * Return reference to element at index
     */
    reference operator [] (size_type index) {
        auto* chunk = chunks_[mask(begin_)];
        if (index >= chunk->end - chunk->begin) {
            index -= chunk->end - chunk->begin; // remove partial chunk size
            const auto skip_chunks = index / kItemsPerChunk;
            chunk = chunks_[mask(begin_ + 1 + skip_chunks)];
        }
        return chunk->items[chunk->mask(chunk->begin + (index & (kItemsPerChunk - 1)))].data;
    }

    /**
     * Return const reference to element at index
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * Return reference to element at index
     * @throws out_of_range
     */
    reference at (size_type index) {
        if (begin_ == end_) {
            throw std::out_of_range("empty");
        }
        auto* chunk = chunks_[mask(begin_)];
        if (index >= chunk->end - chunk->begin) {
            index -= chunk->end - chunk->begin; // remove partial chunk size
            const auto chunk_index = begin_ + 1 + (index / kItemsPerChunk);
            if (chunk_index >= end_) {
                throw std::out_of_range("Bad chunk index");
            }
            chunk = chunks_[mask(chunk_index)];
        }
        auto item_index = chunk->begin + (index & (kItemsPerChunk - 1));
        if (item_index >= chunk->end) {
            throw std::out_of_range("Bad item index");
        }
        return chunk->items[chunk->mask(item_index)].data;
    }

    /**
     * Return const reference to element at position index
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * Return reference to last element
     */
    reference back () {
        auto iter =  end();
        return *--iter;
    }

    /**
     * Return const reference to last element
     */
    const_reference back () const {
        assert(!empty());
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * Return reference to first element
     */
    iterator begin () {
        return iterator(this, begin_);
    }

    /**
     * Return const reference to first element
     */
    const_iterator begin () const {
        return const_iterator(this, begin_);
    }

    // -----
    // clear
    // -----

    /**
     * Remove all elements from this (which are destroyed)
     */
    void clear () {
        for (auto i = begin_; i != end_; ++i) {
            chunks_[mask(i)]->Destroy(a_);
        }
        begin_ = 0;
        end_ = 0;
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * Check if empty (size is 0)
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * Return iterator to end
     */
    iterator end () {
        return iterator(this, end_);
    }

    /**
     * Return const iterator to end
     */
    const_iterator end () const {
        return const_iterator(this, end_);
    }

    // -----
    // erase
    // -----

    /**
     * Remove single element at position
     */
    iterator erase (iterator pos) {
        my_deque q;
        assert(pos.deque_ == this);
        const auto* chunk = chunks_[mask(pos.chunk_index_)];
        const auto index = (pos.chunk_index_ - begin_) * kItemsPerChunk + (pos.item_index_ - chunk->begin);
        std::copy(begin(), pos, std::back_inserter(q));
        std::copy(++pos, end(), std::back_inserter(q));
        swap(q);
        assert(valid());
        return iterator(this, index / kItemsPerChunk, index % kItemsPerChunk);
    }

    // -----
    // front
    // -----

    /**
     * Return reference to first element
     */
    reference front () {
        assert(!empty());
        return *begin();
    }

    /**
     * Return const reference to first element
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * Insert new element before element at specified position
     */
    iterator insert (iterator pos, const_reference v) {
        assert(pos.deque_ == this);
        if (pos.chunk_index_ == end_) {
            push_back(v);
            return end() - 1;
        }
        auto* chunk = chunks_[mask(begin_)];
        auto index = chunk->end - chunk->begin;
        if (pos.chunk_index_ == begin_) {
            index = pos.item_index_;
        } else {
            const auto skip_chunks = pos.chunk_index_ - (begin_ + 1);
            chunk = chunks_[mask(begin_ + 1 + skip_chunks)];
            index += skip_chunks * kItemsPerChunk + (pos.item_index_ - chunk->begin);
        }
        my_deque q;
        std::copy(begin(), pos, std::back_inserter(q));
        q.push_back(v);
        std::copy(pos, end(), std::back_inserter(q));
        swap(q);
        assert(valid());
        return iterator(this, index / kItemsPerChunk, index % kItemsPerChunk);
    }

    // ---
    // pop
    // ---

    /**
     * Remove last element
     */
    void pop_back () {
        assert(!empty());
        auto* last_chunk = chunks_[mask(end_ - 1)];
        assert(last_chunk->begin != last_chunk->end);
        a_.destroy(&last_chunk->items[last_chunk->mask(--last_chunk->end)].data);
        if (last_chunk->begin == last_chunk->end) {
            delete last_chunk;
            --end_;
        }
        assert(valid());
    }


    /**
     * Remove first element
     */
    void pop_front () {
        assert(!empty());
        auto* first_chunk = chunks_[mask(begin_)];
        assert(first_chunk->begin != first_chunk->end);
        a_.destroy(&first_chunk->items[first_chunk->mask(first_chunk->begin)].data);
        ++first_chunk->begin;
        if (first_chunk->begin == first_chunk->end) {
            delete first_chunk;
            ++begin_;
        }
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * Add element at end
     */
    void push_back (const_reference v) {
        EnsureRoomBack();
        auto* last_chunk = chunks_[mask(end_ - 1)];
        a_.construct(&last_chunk->items[last_chunk->mask(last_chunk->end++)].data, v);
        assert(valid());
    }

    /**
     * Add element at beginning
     */
    void push_front (const_reference v) {
        EnsureRoomFront();
        auto* first_chunk = chunks_[mask(begin_)];
        a_.construct(&first_chunk->items[first_chunk->mask(--first_chunk->begin)].data, v);
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * Resize container so that it contains 's' elements; any new elements are value-initalized
     */
    void resize (size_type s) {
        resize(s, T());
        assert(valid());
    }

    /**
     * Resize container so that it contains 'count' elements; any new elements are set to 'v'
     */
    void resize (size_type count, const_reference v) {
        const auto old_size = size();
        if (old_size > count) {
            for (auto i = count; i < old_size; ++i) {
                pop_back();
            }
        } else {
            for (auto i = old_size; i < count; ++i) {
                push_back(v);
            }
        }
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * Return size of container
     */
    size_type size () const {
        const auto chunks_count = end_ - begin_;
        if (chunks_count == 0) {
            return 0;
        } else if (chunks_count ==  1) {
            // Single chunk
            const auto* chunk = chunks_[mask(begin_)];
            return chunk->end - chunk->begin;
        } else {
            const auto* front = chunks_[mask(begin_)];
            const auto* back = chunks_[mask(end_ - 1)];
            return (front->end - front->begin) + (back->end - back->begin) + (chunks_count - 2) * kItemsPerChunk;
        }
    }

    size_type capacity () const {
        return capacity_;
    }

    // ----
    // swap
    // ----

    /**
     * Exchange content of this by content of rhs
     */
    void swap (my_deque& rhs) {
        if(a_ == rhs.a_) {
            std::swap(chunks_, rhs.chunks_);
            std::swap(begin_, rhs.begin_);
            std::swap(end_, rhs.end_);
            std::swap(capacity_, rhs.capacity_);
        }
        else {
            my_deque x(*this);
            *this = rhs;
            rhs = x;
        }
        assert(valid());
    }
};

template <typename T, typename A>
void my_deque<T, A>::NewBackChunk() {
    if (end_ - begin_ + 1 > capacity_) {
        auto new_cap = std::max<size_t>(capacity_ * 2, 1);
        auto* new_chunks = new Chunk*[new_cap];
        auto* p = new_chunks;
        for (auto i = begin_; i != end_; ++i, ++p) {
            *p = chunks_[mask(i)];
        }
        *p++ = new Chunk;
        begin_ = 0;
        end_ = p - new_chunks;
        capacity_ = new_cap;
        delete [] chunks_;
        chunks_ = new_chunks;
    }
    else {
        chunks_[mask(end_++)] = new Chunk;
    }
}

template <typename T, typename A>
void my_deque<T, A>::NewFrontChunk() {
    if (end_ - begin_ + 1 > capacity_) {
        auto new_cap = std::max<size_t>(capacity_ * 2, 1);
        auto* new_chunks = new Chunk*[new_cap];
        auto* p = new_chunks;
        *p++ = new Chunk;
        for (auto i = begin_; i != end_; ++i, ++p) {
            *p = chunks_[mask(i)];
        }
        begin_ = 0;
        end_ = p - new_chunks;
        capacity_ = new_cap;
        delete [] chunks_;
        chunks_ = new_chunks;
    } else {
        chunks_[mask(--begin_)] = new Chunk;
    }
}

#endif // Deque_h
